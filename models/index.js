module.exports = {
  Users:          require('./user').Users,
  Products:       require('./product').Products,
  Subscriptions:  require('./subscription').Subscriptions,
};