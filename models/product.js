const mongoose = require('mongoose'),
      schemas  = require('../schemas');

const products = mongoose.model('Products', schemas.productSchema);

// Extension point for the future functionality

module.exports = {
  Products: products
};
