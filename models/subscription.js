const mongoose = require('mongoose'),
      schemas  = require('../schemas');

const subscriptions = mongoose.model('Subscription', schemas.productSchema);

// Extension point for future functionality

module.exports = {
  Subscriptions: subscriptions
};
