const mongoose = require('mongoose');

module.exports = {
    productSchema: mongoose.Schema({
        category: String,
        model: String,
        shops: [
            {
                shop: String,
                url: String,
                imageUrl: String,
                prices: [{date: Date, price: Number}]
            }
        ]
    }),

    userSchema: mongoose.Schema({
        login: String,
        password: String
    }),

    subscriptionSchema: mongoose.Schema({
        user: mongoose.Schema.Types.ObjectId,
        product: mongoose.Schema.Types.ObjectId,
        notificationPeriod: Number
    })
};
